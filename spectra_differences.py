numbers = []
f = open('data.txt', 'r')
g = open('output.txt', 'w')

for line in f:
    numbers.append(float(line.strip()))

for i in range(0, len(numbers)):
    for j in range(i, len(numbers)):
        if i == j:
            pass
        else:
            g.write("%s\t%s\t%s\n" % (numbers[i], numbers[j], abs(numbers[i] - numbers[j])))
